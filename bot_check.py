import json
import requests
import tweepy
import twitter_credentials
from datetime import datetime
import pytz
import time
import troll_check_parameters
import glob_vars
from tags import get_tags
from key_words import get_keywords


def check_for_troll(username):
    troll_score = 0
    result = {
        "account_created_in_last_30_days": False,
        "randomized_username": False,
        "large_following_to_friends_ratio": False,
        "small_number_of_followers": False,
      "inconsistent_activity": False,
      "large_retweet_to_tweet_ratio": False,
      "followed_by_verified_trolls": False,
      "copying_tweet_content": False,
      "using_registered_keywords": False,
      "using_registered_tags": False,
      "score": 0
    }
    response = {
        "success": True,
        "error": False,
        "error_description": "",
        "result": result
    }
    try:
        auth = tweepy.OAuthHandler(
                twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
        auth.set_access_token(twitter_credentials.ACCESS_TOKEN,
                                twitter_credentials.ACCESS_SECRET_TOKEN)
        api = tweepy.API(auth, wait_on_rate_limit=True)

        user_obj = api.get_user(username)
        user = user_obj._json

        user_timeline = get_large_timeline(user, api)

        if (user.get("followers_count") < 5000):
            user_followers = get_all_followers(user, api)
            # Checking if followed by verified trolls
            if is_followed_by_indexed_accounts(user_followers) is True:

                result["followed_by_verified_trolls"] = True
                troll_score += 1
        # Checking how old is account
        bot_check_start = time.time()
        s = time.time()
        if is_one_month_old(user) is True:
            troll_score += 1
            result["account_created_in_last_30_days"] = True
        t = time.time() - s
        
        # Checking if username is randomized
        if random_username(user) is True:
            
            result["randomized_username"] = True
            troll_score += 1
        # Checking the followers to friends ratio
        if large_followers_to_following_ratio(user) is True:
            
            result["large_following_to_friends_ratio"] = True
            troll_score += 1
        # Checking if account has a small number of followers
        if low_followers_number(user) is True:
            
            result["small_number_of_followers"] = True
            troll_score += 1
        # Checking if account has constant activity
        if const_activity(user_timeline) is False:
            
            result["inconsistent_activity"] = True
            troll_score += 1
        # Checking if account has a high retweet to tweet ratio
        if large_retweet_to_tweet_ratio(user_timeline) is True:
            
            result["large_retweet_to_tweet_ratio"] = True
            troll_score += 1
        # Checking if user is copying tweet content
        if copied_content(user_timeline, username) is True:
            
            result["copying_tweet_content"] = True
            troll_score += 1
        # Checking if user is using registered keywords
        if registered_key_words(username) is True:
            
            result["using_registered_keywords"] = True
            troll_score += 1
        if registered_tags(username) is True:
            
            result["using_registered_tags"] = True
            troll_score += 1
        total_time = time.time() - bot_check_start
        
        result["score"] = troll_score
        response["result"] = result
    except tweepy.TweepError as e:
        response["success"] = False
        response["error"] = True
        response["error_description"] = e.args[0][0]['message']
        
    
    
    return response


def is_one_month_old(user):
    created_at = user.get("created_at")
    days = (datetime.now(pytz.utc) -
            datetime.strptime(created_at, "%a %b %d %H:%M:%S %z %Y")).days
    if days < 30:
        print("%s was created in the last 30 days ago" %
              user.get("screen_name"))
        return True
    else:
        
        return False


"""
To do
"""


def public_file_avatar(user):
    pass


"""
full_info to be reconsidered
"""


def full_info(user):
    if user.get("location") is None:
        return False
    if user.get("default_profile_image") is True:
        return False
    if user.get("profile_use_background_image") is False:
        return False
    return True


def random_username(user):
    length = len(user.get("screen_name"))
    numbers = 0
    for char in user.get("screen_name"):
        try:
            check = int(char) * 2
            numbers += 1
        except:
            pass
    if numbers / length > 0.3:
        return True
    else:
        return False


def large_followers_to_following_ratio(user):
    followers = 0
    following = 0
    try:
        followers = user.get("followers_count")
    except:
        return True
    try:
        following = user.get("friends_count")
    except:
        return True
    if following > 0 and followers / following > troll_check_parameters.followers_to_friends_ratio:
        return True
    else:
        return False


def low_followers_number(user):
    followers = user.get("followers_count")
    if followers < troll_check_parameters.low_followers_count:
        return True
    else:
        return False


def is_followed_by_indexed_accounts(ids):
    trolls = get_trolls()
    diff = set(ids) - set(trolls)
    if len(ids) - len(diff) >= troll_check_parameters.troll_followers_threshold:
        return True
    else:
        return False


def large_retweet_to_tweet_ratio(timeline):
    number_of_retweets = 0
    for tweet in timeline:
        if tweet.get("retweeted_status", None) is not None:
            number_of_retweets += 1
    try:
        if number_of_retweets / len(timeline) > troll_check_parameters.retweet_to_tweet_ratio:
            return True
        else:
            return False
    except:
        return False


def const_activity(timeline):
    tweet_dates = []
    for tweet in timeline:
        date_fields = tweet.get("created_at").split(" ")
        tweet_day = {
            "full_date": tweet.get("created_at"),
            "day": date_fields[2],
            "month": date_fields[1],
            "year": date_fields[5],
          "tweets": 1
        }
        if len(tweet_dates) == 0:
            tweet_dates.append(tweet_day)
        else:
            if check_if_same_day(tweet_dates[-1], tweet_day) is True:
                tweet_dates[-1]["tweets"] += 1
            else:
                tweet_dates.append(tweet_day)
    i = 0
    tmp = 0

    while i < len(tweet_dates)-1:
        tmp += (datetime.strptime(tweet_dates[i].get("full_date"), "%a %b %d %H:%M:%S %z %Y") -
                datetime.strptime(tweet_dates[i+1].get("full_date"), "%a %b %d %H:%M:%S %z %Y")).days
        i += 1

    try:
        result = tmp/i
    except:
        return False

    if result < 7:
        return True
    else:
        return False


def copied_content(timeline, username):
    result = False
    for tweet in timeline:
        if check_if_copied_tweet(tweet.get("content"), username):
            result = True
    return result


def registered_tags(username):
    url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
    tags = get_tags()
    tags_string = ""
    for tag in tags:
        tags_string += tag + ", "
    body = {
        "query": {
            "bool": {
                "must": [
                    {
                      "match" : {
                        "content" : tags_string
                        }
                      },
                  {
                      "match": {
                      "author_username": username
                  }
                      }
                    ]
            }
        }
    }
    body_json = json.dumps(body)
    r = requests.get(url, data=body_json, headers=glob_vars.headers)
    response = json.loads(r.text)
    hits = response.get("hits")
    if hits.get("total") == 0:
        return False
    else:
        return True


def registered_key_words(username):
    url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
    key_words = get_keywords()
    key_words_string = ""
    for key_word in key_words:
        key_words_string += key_word + ", "
    body = {
        "query": {
            "bool": {
                "must": [
                    {
                      "match" : {
                        "content" : key_words_string
                        }
                      },
                  {
                      "match": {
                      "author_username": username
                  }
                      }
                    ]
            }
        }
    }
    body_json = json.dumps(body)
    r = requests.get(url, data=body_json, headers=glob_vars.headers)
    response = json.loads(r.text)
    hits = response.get("hits")
    if hits.get("total") == 0:
        return False
    else:
        return True

# Tools


def check_if_copied_tweet(content, username):
    if content is None:
        return False
    url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
    body = {
        "query": {
            "bool": {
                "must": {
                    "match_phrase": {
                      "content": content
                  }
                    },
                "must_not": {
                  "match": {
                      "author_username": username
                      }
              }
            }
        }
    }
    body_json = json.dumps(body)
    url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
    try:
        r = requests.get(url, headers=glob_vars.headers, data=body_json)
        response = json.loads(r.text)
        # try:
        hits = response.get("hits")
        if hits.get("total") == 0:
            return True
        else:
            return False
    except:
        return False


def check_if_same_day(day1, day2):
    if day1.get("day") != day2.get("day"):
        return False
    elif day1.get("month") != day2.get("month"):
        return False
    elif day1.get("year") != day2.get("year"):
        return False
    else:
        return True


def get_large_timeline(user, api):
    timeline = []
    getting_more_tweets = True
    page_num = 1
    old_number_of_tweets = 0

    while getting_more_tweets is True:
        # , page=page_num))
        tmp = api.user_timeline(user.get("screen_name"),
                                count=200, page=page_num)
        for tweet in tmp:
            timeline.append(tweet._json)
        page_num += 1
        if len(timeline) > old_number_of_tweets:
            old_number_of_tweets = len(timeline)
        else:
            getting_more_tweets = False

    return timeline


def get_all_followers(user, api):
    ids = []
    for page in tweepy.Cursor(api.followers_ids, screen_name=user.get("screen_name")).pages():
        print(f"getting next {len(page)} followers")
        ids.extend(page)
    return ids


def get_trolls():
    url = glob_vars.base_url + glob_vars.twitter_accounts_index + '/_search'
    body = {
        "query": {
            "match": {
                "troll": True
            }
        },
        "size": 100
    }
    body_json = json.dumps(body)
    r = requests.get(url, headers=glob_vars.headers, data=body_json)
    response = json.loads(r.text)
    hits = response.get("hits")
    dict_hits = hits.get("hits")
    list_of_usernames = []
    for twitter_account in dict_hits:
        
        source = twitter_account.get("_source")
        list_of_usernames.append(source.get("id"))
    
    
    return list_of_usernames
