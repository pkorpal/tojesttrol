from glob_vars import base_url, key_words_index, headers
import time
import requests
import json
import logging

# logger = logging.getLogger(__name__)

# logger.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(asctime)s:%(levelname)s: %(message)s")

# file_handler = logging.FileHandler(f"logs/{__name__}.log")
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

def add_keyword(query):
  key_word = query.get("key_word")
  username = query.get("username")
  print(f"Adding keyword {key_word} to index")
  response = {
    "success" : True,
    "error" : False,
    "error_description" : None,
  }
  if check_if_keyword_in_index(key_word) is False:
    try:
      body = {
        "created_at" : time.time(),
        "added_by" : username,
        "key_word" : key_word
      }
      body_json = json.dumps(body)
      url = base_url + key_words_index + "/_doc"
      r = requests.post(url, headers=headers, data=body_json)
      print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
      res = json.loads(r.text)
      result = res.get("result")
      if result != "created":
        response["success"] = False
        response["error"] = True
        response["error_description"] = "Error while adding to index"
    except:
      response["success"] = False
      response["error"] = True
      response["error_description"] = "Error while adding to index"
  else:
    response["success"] = False
    response["error"] = True
    response["error_description"] = "Duplicate"
  print(f"Api response {json.dumps(response, indent=2)}")
  return response

def delete_keyword(query):
  key_word = query.get("key_word")
  print(f"Deleting keyword {key_word} from index")
  response = {
    "key_word" : key_word,
    "deleted" : False,
    "status" : "Error",
    "error_description" : None
  }
  body = {
    "query" : {
      "match": {
        "key_word" : key_word
      }
    }
  }
  body_json = json.dumps(body)

  url = base_url + key_words_index + "/_delete_by_query"
  try:
    r = requests.post(url, headers=headers, data=body_json)
    print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
    res_json = json.loads(r.text)
    deleted = res_json.get("deleted")
    if deleted != 0:
      response["deleted"] = True
      response["status"] = "Success"
  except Exception as e:
    logging.debug(f"Exception {e}")
    response["error_description"] = "Error while deleting tag from index"
  logging.debug(f"Api response {json.dumps(response, indent=2)}")
  return response

def check_if_keyword_in_index(key_word):
  print(f"Checking if keyword {key_word} exists in index")
  body = {
    "query" : {
      "match" : {
        "key_word" : key_word
      }
    },
    "size" : 1
  }
  body_json = json.dumps(body)
  url = base_url + key_words_index + "/_search"
  r = requests.get(url, headers=headers, data=body_json)
  print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
  response = json.loads(r.text)
  hits = response.get("hits")
  total = hits.get("total")
  if total > 0:
    return True
  else:
    return False

def get_keywords(**kwargs):
  size = kwargs.get("limit", 100)
  print(f"Getting keywords from index, limit {size}")
  url = base_url + key_words_index + "/_search"
  key_words = []
  body = {
    "size" : size
  }
  body_json = json.dumps(body)
  r = requests.get(url, headers=headers, data=body_json)
  print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
  response = json.loads(r.text)
  total_hits = response.get("hits")
  hits = total_hits.get("hits")
  for hit in hits:
    source = hit.get("_source")
    key_word = source.get("key_word")
    key_words.append(key_word)
  print(f"Api response {json.dumps(key_words, indent=2)}")
  return key_words

