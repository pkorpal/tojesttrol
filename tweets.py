import tweepy
import twitter_credentials
import json
import requests
from glob_vars import base_url, tweets_index, headers
import datetime

def scrap_tweet(url):
  auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
  auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
  api = tweepy.API(auth)
  response = {
    "status" : "",
    "tweet" : None,
    "error_description" : None
  }
  try:
    tweet_id = url.get("url").split('/')[-1]
    status = api.get_status(tweet_id)._json
    response["tweet"] = get_tweet_data(status)
    response["status"] = "Success"
  except tweepy.TweepError as e:
    response["status"] = "Error"
    response["error_description"] = e.args[0][0]['message']
  return response

def get_tweet_data(tweet):
  user = tweet.get("user")
  quoted_status = tweet.get("quoted_status", None)
  retweeted_status = tweet.get("retweeted_status", None)

  is_retweet = False
  retweet_id = None
  author_username_of_retweet = None
  author_name_of_retweet = None
  retweet_text = None
  
  if quoted_status is not None:
    quoted_user = quoted_status.get("user")
    is_retweet = True
    retweet_id = tweet.get("quoted_status_id")
    author_username_of_retweet = quoted_user.get("screen_name")
    author_name_of_retweet = quoted_user.get("name")
    retweet_text = quoted_status.get("text")
    
  if retweeted_status is not None:
    retweeted_user = retweeted_status.get("user")
    is_retweet = True
    retweet_id = retweeted_status.get("id")
    author_username_of_retweet = retweeted_user.get("screen_name")
    author_name_of_retweet = retweeted_user.get("name")
    retweet_text = retweeted_status.get("text")

  tweet_json = {
    "avatar_url" : user.get("profile_image_url", ""),
    "is_retweet" : is_retweet,
    "in_reply_to" : tweet.get("in_reply_to_screen_name", None),
    "tweet_id" : tweet.get("id", None),
    "retweet_id" : retweet_id,
    "author_username" : user.get("screen_name", None),
    "author_name" : user.get("name", None),
    "author_username_of_retweet" : author_username_of_retweet,
    "author_name_of_retweet" : author_name_of_retweet,
    "content" : tweet.get("text", None),
    "retweet_text" : retweet_text,
    "posted_utime" : tweet.get("created_at", None),
    "thread_count" : 0,
    "retweet_count" : tweet.get("retweet_count", None),
    "reacts" : tweet.get("favorite_count", None),
    "images_urls" : []
  }
  # print(json.dumps(tweet_json, indent=4))
  return tweet_json

def add_tweet_to_index(tweet):
  url = base_url + tweets_index + "/_doc"
  tweet["posted_utime"] = datetime.datetime.strptime(tweet["posted_utime"], "%a %b %d %H:%M:%S %z %Y").timestamp()
  r = requests.post(url, headers=headers, data=json.dumps(tweet))