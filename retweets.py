import json
import requests
import tweepy
import twitter_credentials
import glob_vars

auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
api = tweepy.API(auth, wait_on_rate_limit=True)

def get_retweeters(username): 
  retweeters = []
  body = {
    "query" : {
      "match" : {
        "author_username_of_retweet" : username
      }
    },
    "size": 10000
  }
  replies = []
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")
  for hit in hits.get("hits"):
    replies.append(hit)

  url = glob_vars.base_url + glob_vars.old_tweets_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")

  for hit in hits.get("hits"):
    replies.append(hit)

  for reply in replies:
    tweet = reply.get("_source")
    tmp = tweet.get("author_username")
    index = check_if_already_in_retweeters(tmp, retweeters)
    if index > 0:
      retweeters[index]["number_of_retweets"] += 1
    else:
      retweeter = {
        "username" : tmp,
        "number_of_retweets" : 1
      }
      retweeters.append(retweeter)
  return retweeters

def check_if_already_in_retweeters(username, retweeters):
  index = 0
  if len(retweeters) > 0:
    for index in range(len(retweeters)):
      if retweeters[index].get("username") == username:
        return index
      else:
        index += 1
  else:
    return -1
  return -1

def top_retweeters(username, limit):
  retweeters = get_retweeters(username)
  retweeters_sorted = sorted(retweeters, key=lambda k: k.get("number_of_retweets"), reverse=True)
  # print(json.dumps(retweeters_sorted[:limit], indent=2))
  return retweeters_sorted[:limit]

def get_retweets(id, count):
  retweets = api.retweets(id, count=count)
  for r in retweets:
    pass
    # print(json.dumps(r._json, indent=2))
# top_retweeters("Broniarz", 10)


# get_retweets(id=1116347686809210880, count=10)