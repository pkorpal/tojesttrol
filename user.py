import json
import requests
import tweepy
import twitter_credentials
import glob_vars
from timeit import default_timer as timer

auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
api = tweepy.API(auth, wait_on_rate_limit=True)

def get_users_info(query):
    data = query["users"]
    response = {
        "success" : False,
        "error" : False,
        "error_description" : None,
        "data" : []
    }
    users_info = []
    # start = timer()
    for user in data:
        try:
            u = api.get_user(user)
            tmp = {
                "username" : u._json["screen_name"],
                "followed" : u._json["friends_count"],
                "followers" : u._json["followers_count"],
                "created_at" : u._json["created_at"]
            }
            users_info.append(tmp)
        except:
            tmp = {
                "username" : user,
                "followed" : None,
                "followers" : None,
                "created_at" : None
            }
            users_info.append(tmp)
    # end = timer()
    # total = end - start
    # avg = total / len(data)
    # print(f"Users processed in {total} seconds")
    # print(f"Avg user process time {avg}")
    
    response["data"] = users_info
    return response


