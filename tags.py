from glob_vars import base_url, tags_index, headers
import time
import requests
import json
import logging
from twitter_accounts import stop_stream
from tweets import get_tweet_data, add_tweet_to_index
import tweepy
import twitter_credentials

# logger = logging.getLogger(__name__)

# logger.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(asctime)s:%(levelname)s: %(message)s")

# file_handler = logging.FileHandler(f"logs/{__name__}.log")
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
api = tweepy.API(auth, wait_on_rate_limit=True)

def add_tag(query):
  tag = query.get("tag")
  username = query.get("username")
  print(f"Adding tag {tag} to index")
  response = {
    "success" : True,
    "error" : False,
    "error_description" : None,
  }
  if check_if_tag_in_index(tag) is False:
    try:
      body = {
        "created_at" : time.time(),
        "added_by" : username,
        "tag" : tag
      }
      body_json = json.dumps(body)
      url = base_url + tags_index + "/_doc"
      r = requests.post(url, headers=headers, data=body_json)
      res = json.loads(r.text)
      print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
      result = res.get("result")
      if result != "created":
        response["success"] = False
        response["error"] = True
        response["error_description"] = "Error while adding to index"
    except:
      response["success"] = False
      response["error"] = True
      response["error_description"] = "Error while adding to index"
  else:
    response["success"] = False
    response["error"] = True
    response["error_description"] = "Duplicate"
  print(f"Api response {json.dumps(response, indent=2)}")
  stop_stream()
  return response

def delete_tag(query):
  tag = query.get("tag")
  print(f"Deleting tag {tag} from index")
  response = {
    "tag" : tag,
    "deleted" : False,
    "status" : "Error",
    "error_description" : None
  }
  body = {
    "query" : {
      "match": {
        "tag" : tag
      }
    }
  }
  body_json = json.dumps(body)

  url = base_url + tags_index + "/_delete_by_query"
  try:
    r = requests.post(url, headers=headers, data=body_json)
    res_json = json.loads(r.text)
    print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
    deleted = res_json.get("deleted")
    if deleted != 0:
      response["deleted"] = True
      response["status"] = "Success"
  except Exception as e:
    print(e)
    response["error_description"] = "Error while deleting tag from index"
  print(f"Api response {response}")
  return response

def check_if_tag_in_index(tag):
  print(f"Checking if tag {tag} in index")
  body = { 
    "query" : {
      "match" : {
        "tag" : tag
      }
    },
    "size" : 1
  }
  body_json = json.dumps(body)
  url = base_url + tags_index + "/_search"
  r = requests.get(url, headers=headers, data=body_json)
  print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
  response = json.loads(r.text)
  hits = response.get("hits")
  total = hits.get("total")
  if total > 0:
    return True
  else:
    return False

def get_tags(**kwargs):
  size = kwargs.get("limit", 100)
  print(f"Getting tags from index, limit {size}")
  url = base_url + tags_index + "/_search"
  tags = []
  body = {
    "size" : size
  }
  body_json = json.dumps(body)
  r = requests.get(url, headers=headers, data=body_json)
  print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
  response = json.loads(r.text)
  total_hits = response.get("hits")
  hits = total_hits.get("hits")
  for hit in hits:
    source = hit.get("_source")
    tag = source.get("tag")
    tags.append(tag)
  print(f"Api response {tags}")
  return tags

def get_tweets_with_tag(tag, limit):
  if limit > 1000:
    limit = 1000
  query = {
    "tag" : tag,
    "username" : None
  }
  add_tag(query)
  response = {
    "success" : False,
    "error" : False,
    "error_description" : None,
    "tweets" : []
  }
  tweets = []
  try:
    results = [status for status in tweepy.Cursor(api.search, q=tag).items(limit)]
    for status in results:
      tweet = get_tweet_data(status._json)
      add_tweet_to_index(tweet)
      tweets.append(tweet)
    response["success"] = True
    response["tweets"] = tweets
  except tweepy.TweepError as e:
    response["error_description"] = e.args[0][0]['message']
    response["error"] = True
  return response