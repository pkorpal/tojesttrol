import tweepy
import twitter_credentials
import json
import requests
import datetime
import time
import glob_vars

def get_troll_accounts(limit):
  response = {
    "success" : False,
    "error" : False,
    "error_description" : None,
    "twitter_accounts" : []
  }
  try:
    body = {
      "query" : {
        "match" : {
          "troll" : True
        }
      },
      "size" : limit
    }
    body_json = json.dumps(body)
    url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_search"
    r = requests.get(url, headers=glob_vars.headers, data=body_json)
    result = json.loads(r.text)
    hits = result.get("hits")
    hits_array = hits.get("hits")
    for troll in hits_array:
      t = troll.get("_source")
      tmp = {
        "username" : t.get("username")
      }
      response["twitter_accounts"].append(tmp)
    response["success"] = True
    
  except Exception as e:
    print(e)
    response["error"] = True
    response["error_description"] = ""
  
  return response

def delete_twitter_account_from_index(username):
  """
  Removes given account from index
  """
  response = {
    "username" : username.get("username"),
    "deleted" : False,
    "status" : "Error",
    "error_description" : None
  }
  body = {
    "query" : {
      "match": {
        "username" : username.get("username")
      }
    }
  }
  body_json = json.dumps(body)

  url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_delete_by_query"
  try: 
    r = requests.post(url, headers=glob_vars.headers, data=body_json)
    res_json = json.loads(r.text)
    deleted = res_json.get("deleted")
    if deleted != 0:
      stop_stream()
    response["deleted"] = True
    response["status"] = "Success"
  except Exception as e:
    print(e)
    response["error_description"] = "Error while deleting user from index"
  return response

def check_twitter_account(username):
  """
  Checks if account with given username exists on twitter
  """
  auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
  auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
  api = tweepy.API(auth)
  response = {
    "status" : "Error",
    "username" : username.get("username"),
    "error_description" : None,
    "id" : None,
    "exists" : False
  }
  try:
    user_object = api.get_user(username.get("username"))
    user = user_object._json
    # print(user.get("id"))
    response["id"] = user.get("id")
    response["status"] = "Success"
    response["exists"] = True
  except tweepy.TweepError as e:
    response["description"] = e.args[0][0]['message']
    response["status"] = "Error"
  return response

def add_twitter_account(query):
  auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
  auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
  api = tweepy.API(auth)
  response = {
    "username" : query.get("username"),
    "id" : None,
    "error_description": None,
    "status": "Error"
  }

  try:
    user_object = api.get_user(response.get("username"))
    user = user_object._json
    response["id"] = str(user.get("id"))
  except tweepy.TweepError as e:
    response["error_description"] = e.args[0][0]['message']
    return response

  check = check_if_username_exists_in_index(response.get("username"))
  # print(check)
  if check == True:
    # response["status"] = "Error"
    # response["error_description"] = "Duplicate"
    change_twitter_account_to_observe_true(query["username"], query["troll"], query["bot"])
  else:
    try:
      add_twitter_account_to_index(response.get("username"), response.get("id"), query["troll"], query["bot"])
      response["status"] = "Success"
    except Exception as e:
      print(e)
      response["status"] = "Error"
      response["error_description"] = "Error while adding to index"
  return response

def add_twitter_account_to_index(username, user_id, troll, bot):
  """
  Adds twitter account to index
  """
  body = {
    "username" : username,
    "id" : user_id,
    "troll" : troll,
    "bot" : bot,
    "verified" : True,
    "stream" : False,
    "frontend" : True,
    "observe" : True
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_doc"
  r = requests.post(url, data=body_json, headers=glob_vars.headers)
  # print(r.text)
  stop_stream()

def check_if_username_exists_in_index(username):
  query = {
    "match" : {
      "username" : username
    }
  }
  body = {
    "query" : query,
    "size" : 1
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")
  total_hits = hits.get("total")
  if total_hits == 0:
    # print("No hits when checking if exists in index")
    return False
  else: 
    # print(hits.get("_id"))
    return True

def change_twitter_account_to_observe_true(username, troll, bot):
  query = {
    "match" : {
      "username" : username
    }
  }
  body = {
    "query" : query,
    "size" : 1
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  res = r.json()
  response = res["hits"]["hits"][0]
  stop_stream()

  query = {
    "stream": response["_source"]["stream"],
    "bot": bot,
    "verified": True,
    "troll": troll,
    "id": response["_source"]["id"],
    "frontend": response["_source"]["frontend"],
    "observe": True,
    "username": response["_source"]["username"]
  }
  body_json = json.dumps(query)
  url = glob_vars.base_url + glob_vars.twitter_accounts_index + "/_doc/" + response["_id"]
  r = requests.put(url, headers=glob_vars.headers, data=body_json)
  print(json.dumps(r.json()))

def get_twitter_accounts_of_copied_content(query):
  url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
  body = {
    "query" : {
      "bool" : {
        "must" : [
          {
            "match" : {
              "content" : query["content"]
            }
          }
        ],
        "filter" : 
        {
          "term" : {
            "is_retweet" : False
          }
        }
      }
    },
    "size" : 1000
  }
  body_json = json.dumps(body)
  r = requests.get(url, headers=glob_vars.headers, data=body_json)
  tweets = r.json()["hits"]["hits"]
  twitter_accounts = []
  for tweet in tweets:
    twitter_account = tweet["_source"]["author_username"]
    if twitter_account not in twitter_accounts:
      twitter_accounts.append(twitter_account)
  return twitter_accounts

def stop_stream():
  """
  Stops stream after adding new twitter account to index so stream can restart
  """
  body = {
    "stream_is_alive" : False
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.stream_is_alive_index + "/_doc/" + glob_vars.stream_is_alive_id
  r = requests.post(url, data=body_json, headers=glob_vars.headers)
  # print(r.text)

#######

def get_attacked_accounts(limit):
  response = {
    "success" : False,
    "error" : False,
    "error_description" : None,
    "attacked_account" : []
  }
  try:
    body = {
      "size" : limit
    }
    body_json = json.dumps(body)
    url = glob_vars.base_url + glob_vars.attacked_account_index + "/_search"
    r = requests.get(url, headers=glob_vars.headers, data=body_json)
    result = json.loads(r.text)
    hits = result.get("hits")
    hits_array = hits.get("hits")
    for attacked_account in hits_array:
      ac = attacked_account.get("_source")
      tmp = {
        "username" : ac.get("username")
      }
      response["attacked_account"].append(tmp)
    response["success"] = True
    
  except Exception as e:
    print(e)
    response["error"] = True
    response["error_description"] = ""
  
  return response

def add_attacked_account(username): 
  auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET_KEY)
  auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_SECRET_TOKEN)
  api = tweepy.API(auth)
  response = {
    "username" : username.get("username"),
    "id" : None,
    "error_description": None,
    "status": "Error"
  }

  try:
    user_object = api.get_user(response.get("username"))
    user = user_object._json
    response["id"] = str(user.get("id"))
  except tweepy.TweepError as e:
    response["error_description"] = e.args[0][0]['message']
    return response

  check = check_if_attacked_account_exists_in_index(response.get("username"))
  # print(check)
  if check == True:
    response["status"] = "Error"
    response["error_description"] = "Duplicate"
  else:
    try:
      add_attacked_account_to_index(response.get("username"), response.get("id"))
      response["status"] = "Success"
    except Exception as e:
      print(e)
      response["status"] = "Error"
      response["error_description"] = "Error while adding to index"
  return response

def check_if_attacked_account_exists_in_index(username):
  query = {
    "match" : {
      "username" : username
    }
  }
  body = {
    "query" : query,
    "size" : 1
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.attacked_account_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")
  total_hits = hits.get("total")
  if total_hits == 0:
    # print("No hits when checking if exists in index")
    return False
  else: 
    # print(hits.get("_id"))
    return True

def add_attacked_account_to_index(username, user_id):
  """
  Adds attacked account to index
  """
  body = {
    "username" : username,
    "id" : user_id,
  }
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.attacked_account_index + "/_doc"
  r = requests.post(url, data=body_json, headers=glob_vars.headers)
  # print(r.text)
  stop_stream()

def delete_attacked_account_from_index(username):
  """
  Removes given account from index
  """
  response = {
    "username" : username.get("username"),
    "deleted" : False,
    "status" : "Error",
    "error_description" : None
  }
  body = {
    "query" : {
      "match": {
        "username" : username.get("username")
      }
    }
  }
  body_json = json.dumps(body)

  url = glob_vars.base_url + glob_vars.attacked_account_index + "/_delete_by_query"
  try: 
    r = requests.post(url, headers=glob_vars.headers, data=body_json)
    res_json = json.loads(r.text)
    deleted = res_json.get("deleted")
    if deleted != 0:
      stop_stream()
    response["deleted"] = True
    response["status"] = "Success"
  except Exception as e:
    print(e)
    response["error_description"] = "Error while deleting user from index"
  return response

