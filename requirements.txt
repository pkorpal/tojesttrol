boto3==1.9.101
connexion==2.2.0
Flask==1.0.2
tweepy==3.6.0
psutil==5.5.1
lxml==4.2.5
pytz==2019.1