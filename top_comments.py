import json
import requests
import tweepy
import twitter_credentials
import glob_vars

def get_commenters(username): 
  commenters = []
  body = {
    "query" : {
      "match" : {
        "in_reply_to" : username
      }
    },
    "size": 10000
  }
  replies = []
  body_json = json.dumps(body)
  url = glob_vars.base_url + glob_vars.tweets_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")
  for hit in hits.get("hits"):
    replies.append(hit)

  url = glob_vars.base_url + glob_vars.old_tweets_index + "/_search"
  r = requests.get(url, data=body_json, headers=glob_vars.headers)
  response = json.loads(r.text)
  hits = response.get("hits")

  for hit in hits.get("hits"):
    replies.append(hit)

  for reply in replies:
    tweet = reply.get("_source")
    tmp = tweet.get("author_username")
    index = check_if_already_in_commenters(tmp, commenters)
    if index > 0:
      commenters[index]["number_of_comments"] += 1
    else:
      commenter = {
        "username" : tmp,
        "number_of_comments" : 1
      }
      commenters.append(commenter)
  return commenters

def check_if_already_in_commenters(username, commenters):
  index = 0
  if len(commenters) > 0:
    for index in range(len(commenters)):
      if commenters[index].get("username") == username:
        return index
      else:
        index += 1
  else:
    return -1
  return -1

def top_commenters(username, limit):
  commenters = get_commenters(username)
  commenters_sorted = sorted(commenters, key=lambda k: k.get("number_of_comments"), reverse=True)
  return commenters_sorted[:limit]